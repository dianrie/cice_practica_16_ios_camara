//
//  ViewController.swift
//  Camara
//
//  Created by Diego Angel Fernandez Garcia on 30/03/2019.
//  Copyright © 2019 Diego Angel Fernandez Garcia. All rights reserved.
//

import UIKit


class ShowPhotoViewController: UIViewController {
    var image:UIImage?
    var imageSave:UIImage?
    var position:String?
    
    @IBOutlet weak var mostrarImagen: UIImageView!
    
    @IBOutlet weak var bottonSave: UIButton!
    @IBOutlet weak var boton1: UIButton!
    @IBOutlet weak var boton2: UIButton!
    @IBOutlet weak var boton3: UIButton!
    @IBOutlet weak var boton4: UIButton!
    @IBOutlet weak var boton5: UIButton!
    @IBOutlet weak var boton6: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //Si se seleccino la camara frontal se rota con la opcion espejo la imagen
        if position == "front" {
             image = rotateImage(image: image!)
        }
                
        mostrarImagen.image = image
        imageSave = image
        
        boton1.setImage(image, for: .normal)
        boton2.setImage(filtroSepia(image!, intensity: 1.0, name: "CISepiaTone"), for: .normal)
        boton2.transform = CGAffineTransform(rotationAngle: .pi / 2)
        boton3.setImage(filtroSepia(image!, intensity: 10.0, name: "CISepiaTone"), for: .normal)
        boton3.transform = CGAffineTransform(rotationAngle: .pi / 2)
        boton4.setImage(filtroTransfer(image!, name: "CIPhotoEffectTransfer"), for: .normal)
        boton4.transform = CGAffineTransform(rotationAngle: .pi / 2)
        boton5.setImage(filtroTransfer(image!, name: "CIColorInvert"), for: .normal)
        boton5.transform = CGAffineTransform(rotationAngle: .pi / 2)
        boton6.setImage(filtroTransfer(image!, name: "CIPhotoEffectNoir"), for: .normal)
        boton6.transform = CGAffineTransform(rotationAngle: .pi / 2)

    }
    @IBAction func boton1(_ sender: Any) {
        mostrarImagen.image = image
        imageSave = image
    }
    @IBAction func boton2(_ sender: Any) {
        mostrarImagen.image = filtroSepia(image!, intensity: 1.0, name: "CISepiaTone")
        imageSave = filtroSepia(image!, intensity: 1.0, name: "CISepiaTone")
    }
    @IBAction func boton3(_ sender: Any) {
        mostrarImagen.image = filtroSepia(image!, intensity: 10.0, name: "CISepiaTone")
        imageSave = filtroSepia(image!, intensity: 10.0, name: "CISepiaTone")
    }
    @IBAction func boton4(_ sender: Any) {
        mostrarImagen.image = filtroTransfer(image!, name: "CIPhotoEffectTransfer")
        imageSave = filtroTransfer(image!, name: "CIPhotoEffectTransfer")
    }
    @IBAction func boton5(_ sender: Any) {
       mostrarImagen.image = filtroTransfer(image!, name: "CIColorInvert")
        imageSave = filtroTransfer(image!, name: "CIColorInvert")
    }
    @IBAction func boton6(_ sender: Any) {
       mostrarImagen.image = filtroTransfer(image!, name: "CIPhotoEffectNoir")
        imageSave = filtroTransfer(image!, name: "CIPhotoEffectNoir")
    }

    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: newSize.width, height: newSize.height)))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func filtroSepia(_ input: UIImage, intensity: Double, name: String) -> UIImage?
    {
        let ciImage = CIImage(image: input)
        let sepiaFilter = CIFilter(name: name)
        sepiaFilter?.setValue(ciImage, forKey: kCIInputImageKey)
        sepiaFilter?.setValue(intensity, forKey: kCIInputIntensityKey)
        let image = UIImage(ciImage: (sepiaFilter?.outputImage)!)
        return image
    }
    func filtroTransfer(_ input: UIImage, name: String) -> UIImage?
    {
        let ciImage = CIImage(image: input)
        let filter = CIFilter(name: name)
        filter?.setValue(ciImage, forKey: kCIInputImageKey)
        let image = UIImage(ciImage: (filter?.outputImage)!)
        return image
    }
    
    func rotateImage(image:UIImage) -> UIImage
    {
        var rotatedImage = UIImage()
        rotatedImage = UIImage(cgImage: image.cgImage!, scale: 1.0, orientation: .leftMirrored)
        
        return rotatedImage
    }
    // MARK: - Action methods
    
    @IBAction func save(sender: UIButton) {
        guard let imageToSave = imageSave else {
            return
        }
        //Guardamos el UIImage en nuestro CameraRoll
        UIImageWriteToSavedPhotosAlbum(imageToSave, nil, nil, nil)
        dismiss(animated: true, completion: nil)
        
    }
    
    
}

