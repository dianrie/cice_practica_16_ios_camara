//
//  UIImage+Scale.swift
//  FirebaseDemo
//
//  Created by formador on 29/3/19.
//  Copyright © 2019 355 Berry Street. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    func escalarImagen(nuevaAnchura: CGFloat) -> UIImage {
        //Comprobamos si la imagen ya tiene esa anchura
        if self.size.width == nuevaAnchura {
            return self
        }
        
        //Re-escalamos la imagen
        let factorEscala = nuevaAnchura / self.size.width
        let nuevaAltura = self.size.height * factorEscala
        let nuevoTamañoImagen = CGSize(width: nuevaAnchura, height: nuevaAltura)
        
        UIGraphicsBeginImageContextWithOptions(nuevoTamañoImagen, false, 0.0)
        self.draw(in: CGRect(x: 0, y: 0, width: nuevaAnchura, height: nuevaAltura))
        
        let nuevaImagen: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return nuevaImagen ?? self
    }
}
