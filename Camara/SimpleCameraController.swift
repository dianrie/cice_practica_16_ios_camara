//
//  TomarFotoViewController.swift
//  Camara
//
//  Created by Diego Angel Fernandez Garcia on 30/03/2019.
//  Copyright © 2019 Diego Angel Fernandez Garcia. All rights reserved.
//

import UIKit
import AVFoundation

class SimpleCameraController: UIViewController {
    
    @IBOutlet var cameraButton:UIButton!
    
    let captureSession = AVCaptureSession()
    
    var backFacingCamera: AVCaptureDevice?
    var frontFacingCamera: AVCaptureDevice?
    var currentDevice: AVCaptureDevice?
    
    var stillImageOutput: AVCapturePhotoOutput?
    var stillImage: UIImage?
    
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    
    var toggleCamera = UISwipeGestureRecognizer()
    var zoomInGesture = UISwipeGestureRecognizer()
    var zoomOutGesture = UISwipeGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        configGestureRecognizer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Configuration
    func config() {
        //Definimos cómo ajustar la sesión
        //Presets
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        
        //Decimos a nuestra sesión las cámaras que queremos usar
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera, .builtInTrueDepthCamera, .builtInTelephotoCamera, .builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        
        if (deviceDiscoverySession.devices.count) <= 0 {
            print("No tienes cámara")
        }
        //Comprobar que tenemos cámara
        guard let _ = deviceDiscoverySession.devices.first else { return }
        
        print(deviceDiscoverySession.devices.count)
        
        
        
        //Guardamos cada cámara en su
        for device in (deviceDiscoverySession.devices) {
            if device.position == .back {
                backFacingCamera = device
            } else if device.position == .front {
                frontFacingCamera = device
            }
        }
        
        //Asignamos la cámara trasera como la actual (la que estamos usando)
        currentDevice = backFacingCamera
        
        //Configuramos el Output
        stillImageOutput = AVCapturePhotoOutput()
        
        //Configuramos el Input
        guard let captureDeviceInput = try? AVCaptureDeviceInput(device: currentDevice!) else { return }
        
        //Asignamos ambos a la sesión
        captureSession.addInput(captureDeviceInput)
        captureSession.addOutput(stillImageOutput!)
        
        //Creamos el Preview
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraPreviewLayer?.frame = view.layer.frame
        view.layer.addSublayer(cameraPreviewLayer!)
        
        //Traemos el botón al frente
        view.bringSubviewToFront(cameraButton)
        
        //Iniciamos la sesión
        captureSession.startRunning()
    }
    
    func configGestureRecognizer() {
        toggleCamera.direction = .up
        toggleCamera.addTarget(self, action: #selector(changeCamera))
        view.addGestureRecognizer(toggleCamera)
        
        zoomInGesture.direction = .right
        zoomInGesture.addTarget(self, action: #selector(zoomIn))
        view.addGestureRecognizer(zoomInGesture)
        
        zoomOutGesture.direction = .left
        zoomOutGesture.addTarget(self, action: #selector(zoomOut))
        view.addGestureRecognizer(zoomOutGesture)
    }
    
    @objc func changeCamera() {
        print("Gesto detectado")
        //Para reajustar la sesión tenemos que re-configurarla
        captureSession.beginConfiguration()
        //Cambiamos el dispositivo de captura (la cámara)
        let newDevice = currentDevice?.position == AVCaptureDevice.Position.back ? frontFacingCamera : backFacingCamera
        
        //Eliminamos los inputs de la sesión
        for input in captureSession.inputs {
            captureSession.removeInput((input as! AVCaptureInput))
        }
        
        //Creamos un nuevo input
        let cameraInput: AVCaptureDeviceInput
        do {
            cameraInput = try AVCaptureDeviceInput(device: newDevice!)
        } catch {
            print(error)
            return
        }
        
        //Le añadimos el input a la sesión comprobando antes que se pueda
        if captureSession.canAddInput(cameraInput) {
            captureSession.addInput(cameraInput)
        }
        
        currentDevice = newDevice
        //Guardamos los cambios
        captureSession.commitConfiguration()
    }
    
    @objc func zoomIn() {
        print("Zoom In")
        //Comprobamos qué zoom tiene aplicado el dispositivo
        if let zoomFactor = currentDevice?.videoZoomFactor {
            print(zoomFactor)
            if zoomFactor < 5.0 {
                let newZoomFactor = min(zoomFactor + 1, 5.0)
                do {
                    try currentDevice?.lockForConfiguration()
                    currentDevice?.ramp(toVideoZoomFactor: newZoomFactor, withRate: 1.0)
                    currentDevice?.unlockForConfiguration()
                } catch {
                    print(error)
                    return
                }
            }
        }
    }
    
    @objc func zoomOut() {
        print("Zoom Out")
        //Comprobamos qué zoom tiene aplicado el dispositivo
        if let zoomFactor = currentDevice?.videoZoomFactor {
            print(zoomFactor)
            if zoomFactor > 1.0 {
                let newZoomFactor = max(zoomFactor - 1.0, 1.0)
                do {
                    try currentDevice?.lockForConfiguration()
                    currentDevice?.ramp(toVideoZoomFactor: newZoomFactor, withRate: 1.0)
                    currentDevice?.unlockForConfiguration()
                } catch {
                    print(error)
                    return
                }
            }
        }
    }
    
    // MARK: - Action methods
    
    @IBAction func capture(sender: UIButton) {
        //Establecemos los ajustes de la foto
        let photoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        photoSettings.isAutoStillImageStabilizationEnabled = true
        photoSettings.isHighResolutionPhotoEnabled = true
        //photoSettings.flashMode = .auto
        
        stillImageOutput?.isHighResolutionCaptureEnabled = true
        //Hacemos la foto usando los ajustes que acabamos de establecer
        stillImageOutput?.capturePhoto(with: photoSettings, delegate: self)
    }
    
    // MARK: - Segues
    
    @IBAction func unwindToCameraView(segue: UIStoryboardSegue) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPhoto" {
            let photoVC = segue.destination as! ShowPhotoViewController
            photoVC.image = stillImage
            if self.currentDevice!.position == .back {
                photoVC.position = "back"
            } else if self.currentDevice!.position == .front {
                photoVC.position = "front"
            }
        }
    }
}

extension SimpleCameraController: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        //Comprobamos que no tengamos un error
        guard error == nil else {
            return
        }
        
        //Obtenemos la imagen capturada
        guard let imageData = photo.fileDataRepresentation() else {
            return
        }
        
        stillImage = UIImage(data: imageData)
        performSegue(withIdentifier: "showPhoto", sender: self)
    }
    
}
